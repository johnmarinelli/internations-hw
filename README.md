# John Marinelli Internations Coding Challenge 
Hi!  For this app, I built the beginnings of a simple user/group CMS.  The main view is a view of users in each group, or, of groups for each user.  You can toggle it using a button.  To start the app, you need to have Docker running.

## Initialization
Clone the repo.
Install dependencies on backend and client: `npm install; cd client; npm install; cd ..`
In one terminal, run `docker-compose up`.  This will start the MongoDB, Redis, and backend API services, and seed the database. 
In another terminal, `cd client` and `npm start`.  This should start a Webpack dev server on port 3000.  

## To test (backend only)
`npm run test`

## Features
Basic create/read/delete functionalities for users and groups.  ES6 syntax.  API tests.  Simple redis caching for `/api/users`.  Responsive.  

## How this app is structured
Root directory is a JSON API built with Node, Express, and MongoDB.  `client/` is a React frontend.  

Only the backend has tests.  I added tests for the endpoints and most of the model methods.  The test directory is `test/`.

Backend file structure:
```
▾ app/
  ▾ models/
      group.js
      user.js
  ▾ routes/
    ▾ groups/
        all.js
        destroy.js
        index.js
        new.js
    ▾ users/
        all.js
        destroy.js
        index.js
        new.js
      index.js
▾ test/
  ▾ models/
      group.js
      user.js
    group.js
    seed.js
    user.js
```

Client file structure:
```
▾ src/
    App.css
    App.js
    App.test.js
    Group.js
    GroupContainer.js
    index.css
    index.js
    logo.svg
    registerServiceWorker.js
    User.js
    UserContainer.js
```

## API Endpoints

GET /api/users
Gets a list of users.  Can use `age_min` and `age_max` in query to filter.  

GET /api/groups
Gets all groups.

POST /api/users
Create a user.  Pass `name`, `email`, and `age` in query to set user name, email and age.

POST /api/groups
Create a group.  Pass `name` in query to set group name.

DELETE /api/users/:email
Delete user by email.

DELETE /api/groups
Delete a group.  Pass `name` in request body.
