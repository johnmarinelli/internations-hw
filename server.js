let isTest = process.env.NODE_ENV === 'test';

let express    = require('express');
let bodyParser = require('body-parser');
let app        = express();
let routes = require('./app/routes');
let morgan     = require('morgan');
let mongoose   = require('mongoose');

let port = isTest ? 8081 : (process.env.PORT || 8080); // set our port

/*
 * configure app
 */
app.use(morgan('dev')); 
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());


/*
 * connect to services
 */
mongoose.connect(process.env.MONGO_URL); 

/*
 * register routes
 */
app.use('/api', routes);

/*
 * start app
 */
app.listen(port);
console.log('Magic happens on port ' + port);

if (isTest) {
  module.exports = app;
}
