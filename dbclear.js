let mongoose = require('mongoose');
mongoose.connect(process.env.MONGO_URL); 
mongoose.Promise = global.Promise;

let User = require('./app/models/user');
let Group = require('./app/models/group');

let promises = [
  User.remove({}, (err) => {
    if (err) return;
  }),

  Group.remove({}, (err) => {
    if (err) return;
  })
];

Promise.all(promises)
  .then(() => {
    console.log('Database cleared.');
    mongoose.connection.close();
    process.exit();
  });

