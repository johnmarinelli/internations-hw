process.env.NODE_ENV = 'test';

let User = require('../app/models/user');
let Group = require('../app/models/group');
let { teardown, seedApp } = require('./seed');

let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../server');
let should = chai.should();
let expect = chai.expect;

describe('Groups', () => {
  beforeEach((done) => {
    seedApp(done);
  });

  /*
   * test GET groups
   */

  describe('GET api/groups', () => {
    it('should retrieve a list of groups.', (done) => {
      chai.request(server)
        .get('/api/groups')
        .end((err, res) => {
          res.should.have.status(200);

          let body = res.body;
          expect(body.length).to.equal(3);
          done();
        });
    });

  });

  /*
   * test POST groups
   */
  describe('POST api/groups', () => {

    it('should create a group given valid input.', (done) => {
      chai.request(server)
        .post('/api/groups')
        .send({ name: 'Fourth Group' })
        .end((err, res) => {
          res.should.have.status(200);
          expect(res.body.message).to.equal('Group created!');
          done();
        });
    });

  });

  /*
   * test DELETE groups
   */

  describe('DELETE api/groups', () => {
    it('should delete a group if it\'s empty', (done) => {
      chai.request(server)
        .delete('/api/groups')
        .send({ name: 'First Group' })
        .end((err, res) => {
          res.should.have.status(200);

          Group.find({ name: 'First Group' }, (err, group) => {
            expect(group.length).to.equal(0);
            done();
          });
        });
    });

    it('should not delete a group if it\'s not empty', (done) => {

      Group.findOne({ name: 'First Group' }, (err, group) => {
        User.findOne({ email: 'pablo.feeney@gmail.com' }, (err, user) => {
          group.addUser(user, (group) => true);
          user.addToGroup(group, (user) => true);

          chai.request(server)
            .delete('/api/groups')
            .send({ name: 'First Group' })
            .end((err, res) => {
              res.should.have.status(400);

              Group.find({ name: 'First Group' }, (err, group) => {
                expect(group.length).to.equal(1);
                done();
              });
            });

        });

      });

    });
  });

  afterEach((done) => {
    teardown(done);
  });
});

