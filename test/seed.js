let mongoose = require('mongoose');
let User = require('../app/models/user');
let Group = require('../app/models/group');

mongoose.Promise = global.Promise;

let seedApp = (done) => {

  let promises = [];
  let handleError = (err) => {
    if (err) {
      console.log('Error while seeding test database:');
      console.error(err.message);
      throw new Error(err.message);
    }
  };

  /*
   * Fixtures
   */

  let groups = [
    {"name": "First Group"},
    {"name": "Second Group"},
    {"name": "Third Group"}
  ].map((group) => {
    let g = new Group();
    g.name = group['name'];

    let promise = g.save((err) => {
      handleError(err);
    });

    promises.push(promise);
    return g;
  });

  let seedUsers = () => { 
    promises = [];

    let users = [
      {"age":52,"email":"Stanley.Heaney@gmail.com","name":"Mr. Scot Corkery"},
      {"age":48,"email":"Pablo.Feeney@gmail.com","name":"Alexa Bergstrom"},
      {"age":27,"email":"Javon.Rippin80@gmail.com","name":"Dr. Bernice Kunze"},
      {"age":55,"email":"Curtis17@hotmail.com","name":"Aliza Parker"},
      {"age":51,"email":"Jaqueline.Dare@gmail.com","name":"Macey Buckridge"}
    ];
    
    for (let i = 0; i < users.length; ++i) {
      let user = users[i];
      let u = new User();
      u.name = user['name'];
      u.email = user['email'];
      u.age = user['age'];

      let promise = u.save((err) => {
        handleError(err);
        return u;
      });

      promises.push(promise);
    }

    // seed users
    Promise.all(promises)
      .then((data) => {
        done();
      })
      .catch(console.error);
  };

  // seed groups
  Promise.all(promises)
    .then(seedUsers)
    .catch(console.error);
};

let teardown = (done) => {
  let promises = [
    User.remove({}, () => true),
    Group.remove({}, () => true)
  ];
  Promise.all(promises).then(() => done());
}

module.exports = {
  seedApp: seedApp,
  teardown: teardown
};
