process.env.NODE_ENV = 'test';

let User = require('../../app/models/user');
let Group = require('../../app/models/group');
let chai = require('chai');
let should = chai.should();
let expect = chai.expect;

describe('User model', () => {

  it('should add a group to a user', () => {
    let user = new User();
    user.name = 'name';
    user.email = 'email@email.com';
    user.age = 50;

    let group = new Group();
    group.name = 'group';

    user.addToGroup(group, (user) => {
      expect(user._groups.length).to.equal(1);
    });
  });

  it('should remove a group from a user', (done) => {
    let user = new User();
    user.name = 'name';
    user.email = 'email@email.com';
    user.age = 50;

    let group = new Group();
    group.name = 'group';

    user._groups.addToSet(group._id);

    Promise.all([
      user.save((err) => true),
      group.save((err) => true)
    ]).then(() => {

      expect(user._groups.length).to.equal(1);

      user.removeFromGroup(group, (user) => {
        expect(user._groups.length).to.equal(0);
        done();
      });

    })
  });

  afterEach((done) => {
    User.remove({}, (err) => done());
  });

});

