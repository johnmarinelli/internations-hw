process.env.NODE_ENV = 'test';

let User = require('../../app/models/user');
let Group = require('../../app/models/group');
let chai = require('chai');
let should = chai.should();
let expect = chai.expect;

describe('Group model', () => {

  it('should add a user to a group', (done) => {
    let user = new User();
    user.name = 'name';
    user.email = 'email@email.com';
    user.age = 50;

    let group = new Group();
    group.name = 'group';

    group.addUser(user, (group) => {
      expect(group.usersInGroup.length).to.equal(1);
      done();
    });

  });

  it('should find a group by name', (done) => {
    let group = new Group();
    group.name = 'group';

    group.save((err) => true)
      .then(() => {
        Group.findByName('group', (err, group) => {
          expect(group.name).to.equal('group');
          done();
        });
      });
  });

  it('should delete a group if it\'s empty', (done) => {
    let group = new Group();
    group.name = 'group';

    group.save((err) => true)
      .then(() => {
        Group.removeIfEmpty('group', (success) => {
          expect(success).to.equal(true);
          done();
        });
      });
  });

  afterEach((done) => {
    Promise.all([
      User.remove({}, (err) => true),
      Group.remove({}, (err) => true)
    ]).then(() => done());
  });
});

