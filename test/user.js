process.env.NODE_ENV = 'test';

let User = require('../app/models/user');
let Group = require('../app/models/group');
let { teardown, seedApp } = require('./seed');

let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../server');
let should = chai.should();
let expect = chai.expect;

chai.use(chaiHttp);

describe('Users', () => {
  beforeEach((done) => {
    seedApp(done);
  });

  /*
   * test GET users
   */
  describe('GET api/users', () => {
    it('should retrive a list of users.', (done) => {
      chai.request(server)
        .get('/api/users')
        .end((err, res) => {
          res.should.have.status(200);

          let body = res.body;
          expect(body.length).to.equal(5);
          done();
        });
    });

    it('should retrive a list of users filtered by age.', (done) => {
      chai.request(server)
        .get('/api/users?age_min=18&age_max=30')
        .end((err, res) => {
          res.should.have.status(200);

          let body = res.body;
          expect(body.length).to.equal(1);
          done();
        });
    });

    it('should retrieve a list of users given a min age.', (done) => {
      chai.request(server)
        .get('/api/users?age_min=30')
        .end((err, res) => {
          res.should.have.status(200);

          let body = res.body;
          expect(body.length).to.equal(4);
          done();
        });
    });

    it('should retrieve a list of users given a max age.', (done) => {
      chai.request(server)
        .get('/api/users?age_max=30')
        .end((err, res) => {
          res.should.have.status(200);

          let body = res.body;
          expect(body.length).to.equal(1);
          done();
        });
    });
  });

  describe('POST api/users', () => {
    it('should not create a user given invalid input.', (done) => {
      chai.request(server)
        .post('/api/users')
        .send({ name: 'new user', age: 20, email: 'newuser'})
        .end((err, res) => {
          res.should.have.status(400);
          done();
        });
    });

    it('should create a user given valid input.', (done) => {
      chai.request(server)
        .post('/api/users')
        .send({ name: 'new user', age: 20, email: 'newuser@email.com'})
        .end((err, res) => {
          res.should.have.status(200);
          expect(res.body.message).to.equal('User created!');
          done();
        });
    });

    it('should associate user with group, if passed a group name', (done) => {
      chai.request(server)
        .post('/api/users')
        .send({ name: 'new user', age: 20, email: 'newuser@email.com', group: 'First Group' })
        .end((err, res) => {
          res.should.have.status(200);
          expect(res.body.message).to.equal('User created!');

          User.findOne({ name: 'new user' }, (err, user) => {
            expect(user._groups.length).to.equal(1);
          });

          done();
        });
    });


    it('should not create a user if email is not unique.', (done) => {
      chai.request(server)
        .post('/api/users')
        .send({ name: 'new user', age: 20, email: 'Stanley.Heaney@gmail.com'})
        .end((err, res) => {
          res.should.have.status(400);
          done();
        });
    });
  });

  describe('DELETE api/users/', () => {
    it('should delete a user given their email', (done) => {
      chai.request(server)
        .delete('/api/users/Stanley.Heaney@gmail.com')
        .end((err, res) => {
          res.should.have.status(200);

          User.find({ email: 'Stanley.Heaney@gmail.com' }, (err, user) => {
            expect(user.length).to.equal(0);
          });

          done();
        });
        
    });
  });

  afterEach((done) => {
    teardown(done);
  });
});

