let mongoose = require('mongoose');
let faker = require('faker');
let User = require('./app/models/user');
let Group = require('./app/models/group');

const userCount = 10,
  groupCount = 5;

mongoose.connect(process.env.MONGO_URL); 
mongoose.Promise = global.Promise;

let groupPromises = [];
let userPromises = [];
let handleError = (err) => {
  if (err) {
    console.log('Error while seeding database:');
    console.error(err.message);
    throw new Error(err.message);
  }
}

for (let i = 0; i < groupCount; ++i) {
  let group = new Group();
  group.name = faker.company.companyName();

  let promise = group.save((err) => {
    handleError(err);
    return group;
  });

  groupPromises.push(promise);
}

for (let i = 0; i < userCount; ++i) {
  let user = new User();
  user.name = faker.name.findName();
  user.email = faker.internet.email();
  user.age = Math.floor(Math.random() * 20) + 20

  let promise = user.save((err) => {
    handleError(err);
    return user;
  });

  userPromises.push(promise);
}

Promise.all(groupPromises.concat(userPromises))
  .then((data) => {
    Group.find({}, (err, groups) => {
      handleError(err);

      // associate groups with users
      User.find({}, (err, users) => {
        handleError(err);
        let promises = [];

        users.forEach((user) => {

          let rand = Math.floor(Math.random() * groups.length);
          let group = groups[rand];
        
          let userPromise = user.addToGroup(group, (user) => null );
          let groupPromise = group.addUser(user, (group) => null);

          promises.push(userPromise);
          promises.push(groupPromise);
          
          Promise.all(promises)
            .then((data) => {
              mongoose.connection.close();
              process.exit();
            })
            .catch(handleError);
        });
      });
    });
    
  })
  .catch(handleError);


