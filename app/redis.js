let redis = require('redis');
let redisClient = redis.createClient({ url: process.env.REDIS_URL });
redisClient.on('error', (err) => {
  console.log('redis error:');
  console.log(err);
  console.error(err);
});

module.exports = redisClient;
