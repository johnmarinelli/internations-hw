let routes = require('express').Router();
let users = require('./users');
let groups = require('./groups');

routes.get('/', (req, res) => {
  res.json('Welcome to my API');
});

routes.use('/users', users);
routes.use('/groups', groups);
module.exports = routes;
