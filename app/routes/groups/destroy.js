let Group = require('../../models/group');

module.exports = (req, res) => {
  let name = req.body.name;

  Group.removeIfEmpty(name, (success) => {
    if (!success) {
      res.status(400).json({ message: `Group ${name} not empty.` });
    }
    else {
      res.json({ message: `Group ${name} successfully deleted.` });
    }
  });

};
