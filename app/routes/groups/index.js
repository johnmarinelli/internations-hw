let models = require('express').Router();
let Joi = require('joi');
let validate = require('express-validation');

const all = require('./all');
const create = require('./new');
const destroy = require('./destroy');

/*
 * #all
 */
models.get('/', all);

/*
 * #create
 */
const groupCreateValidation = {
  body: {
    name: Joi.string().min(3).max(25).required()
  }
};
models.post('/', validate(groupCreateValidation), create);

/*
 * #destroy
 */
const groupDestroyValidation = {
  body: {
    name: Joi.string().required()
    //auth_token: Joi.string().regex(/^john$/)
  }
};
models.delete('/', validate(groupDestroyValidation), destroy);

module.exports = models;
