let Group = require('../../models/group');

module.exports = (req, res) => {
  let group = new Group();
  group.name = req.body.name;

  return group.save(function(err) {
    if (err)
      res.status(400).send(err);

    res.json({ message: 'Group created!' });
  });
};
