let Group = require('../../models/group');

module.exports = (req, res) => {
  if (req.query.includeUsers && 'true' === req.query.includeUsers) {
    Group.withUsers((err, groupsAndUsers) => {
      if (err) {
        res.send(err);
      }

      res.json(groupsAndUsers);
    });
  } 

  else {
    Group.find({}, (err, groups) => {
      if (err) {
        res.send(err);
      }

      res.json(groups);
    });
  }
};
