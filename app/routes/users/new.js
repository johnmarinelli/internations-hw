let User = require('../../models/user');
let Group = require('../../models/group');

module.exports = (req, res) => {
  let user = new User();
  user.name = req.body.name;
  user.email = req.body.email;
  user.age = req.body.age;

  if (req.body.group) {
    Group.findOne({ name: req.body.group }, (err, group) => {
      if (err) {
        res.status(400).send(err);
      }

      // add user to group
      user.addToGroup(group, (user) => true);
      group.addUser(user, (group) => true);

    });
  }

  return user.save(function(err) {
    if (err)
      res.status(400).send(err);

    res.json({ message: 'User created!' });
  });
};
