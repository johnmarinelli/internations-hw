let User = require('../../models/user');
let redisClient = require('../../redis');

/*
 * get /users
 * returns a list of users.
 * parameters:
 *  - res.body.email_regex: returns users whose emails match regex
 */
module.exports = (req, res) => {

  redisClient.get(req.url, (err, redisRes) => {
    if (redisRes) {
      console.log('Retrieving from cache');
      res.json(JSON.parse(redisRes));
    }
    else {
      let userFilter = {};

      if (req.query.age_min || req.query.age_max) {
        let min = (+req.query.age_min) || -1,
          max = (+req.query.age_max) || Number.MAX_SAFE_INTEGER;
        userFilter['age'] = { $gt: min, $lt: max };
      }

      if (req.query.includeGroups && 'true' === req.query.includeGroups) {
        const match = {};
        match['$match'] = userFilter;

        User.withGroups(match, (err, usersAndGroups) => {
          if (err)
            res.send(err);

          // cache this url
          // expires in 1 minute
          redisClient.setex([req.url, 60, JSON.stringify(users)]);

          res.json(usersAndGroups);
        });
      }

      else {
        User.find(userFilter, (err, users) => {
          if (err)
            res.send(err);

          // cache this url
          // expires in 1 minute
          redisClient.setex([req.url, 60, JSON.stringify(users)]);

          res.json(users);
        });
      }
    }
  });

};
