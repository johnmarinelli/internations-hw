let User = require('../../models/user');

module.exports = (req, res) => {
  let email = req.body.email;
  User.remove({ email: email }, (err, result) => {
    if (err)
      res.status(400).send(err);

    res.json({ message: `User ${email} successfully deleted.`, result});
  });
};
