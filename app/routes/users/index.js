let models = require('express').Router();
let Joi = require('joi');
let validate = require('express-validation');

const all = require('./all');
const create = require('./new');
const destroy = require('./destroy');

/*
 * #all
 */
models.get('/', all);

/*
 * #create
 */
const userCreateValidation = {
  body: {
    email: Joi.string().email().required(),
    age: Joi.number().required(),
    group: Joi.string().min(3).max(25)
  }
};

models.post('/', validate(userCreateValidation), create);

/*
 * #destroy
 */
const userDestroyValidation = {
  params: {
    email: Joi.string().email().required()
  },

  body: {
    //auth_token: Joi.string().regex(/^john$/)
  }
};

models.delete('/:email', validate(userDestroyValidation), destroy);

module.exports = models;

