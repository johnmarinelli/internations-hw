let mongoose = require('mongoose');
let Group = require('./group');
let Schema = mongoose.Schema;

let UserSchema = new Schema({
	name: String,
  email: { type: String, lowercase: true, unique: true },
  age: Number,
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now },
  _groups: [{ type: Schema.ObjectId, ref: 'Group' }]
});

UserSchema.methods.addToGroup = function (group, cb) {
  // add group to user
  this._groups.addToSet(group._id);

  return this.save((err) => {
    if (err) {
      console.log('user.addToGroup - error');
      console.log(err.message);
      throw new Error(err.message);
    }
    cb(this);
  });

};

UserSchema.methods.removeFromGroup = function (group, cb) {
  // add group to user
  this._groups.pull(group._id);

  return this.save((err) => {
    if (err) {
      console.log('user.removeFromGroup - error');
      console.log(err.message);
      throw new Error(err.message);
    }
    cb(this);
  });

};

UserSchema.statics.withGroups = function (match, cb) {
  const lookup = {
    "$lookup": {
      "localField": "_groups",
      "from": "groups",
      "foreignField": "_id",
      "as": "userGroups"
    }
  };

  return this.aggregate([match, lookup], (err, usersAndGroups) => {
    if (err) {
      console.log('Group::withUsers - error');
      console.log(err.message);
    }

    cb(err, usersAndGroups);
  });
};

module.exports = mongoose.model('User', UserSchema);
