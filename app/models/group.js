let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let GroupSchema = new Schema({
  name: { type: String, unique: true },
  usersInGroup: [{ type: Schema.Types.ObjectId, ref: 'User' }]
});

GroupSchema.statics.findByName = function (name, cb) {
  return this.findOne({ name: name }, cb);
};

GroupSchema.statics.removeIfEmpty = function (name, cb) {
  this.findOne({ name: name }, (err, group) => {
    if (err) {
      console.log('group.addUser - error');
      console.log(err.message);
    }

    if (group && group.usersInGroup.length === 0) { 
      this.remove({ name: name }, (err) => {
        if (!err) return cb(true);
      });
    }

    else return cb(false);
  });

};

GroupSchema.methods.addUser = function (user, cb) {
  this.usersInGroup.addToSet(user._id);

  return this.save((err) => {
    if (err) {
      console.log('group.addUser - error');
      console.log(err.message);
    }
    cb(this);
  });
};

GroupSchema.statics.withUsers = function (cb) {
  const query = {
    "$lookup" : {
      "localField": "usersInGroup",
      "from": "users",
      "foreignField": "_id",
      "as": "groupUsers"
    }
  };

  return this.aggregate([query], (err, groupsAndUsers) => {
    if (err) {
      console.log('Group::withUsers - error');
      console.log(err.message);
    }

    cb(err, groupsAndUsers);
  });
};

module.exports = mongoose.model('Group', GroupSchema);
