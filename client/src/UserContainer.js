import React, { Component } from 'react';
import { ListGroup } from 'react-bootstrap';
import {List, Card, CardHeader, CardTitle } from 'react-bootcards';
import Group from './Group';

class UserContainer extends Component {
  constructor (props) {
    super(props);
    this.state = {
      name: props.name,
      groups: props.groups
    }
  }

  render () {
    let groups = [];

    if (this.state.groups) {
      groups = (
        <div>
          {this.state.groups.map(group => 
            <Group key={group._id} name={group.name} />
          )}
        </div>
      );
    }

    return (
      <Card>
        <CardHeader className="clearfix">
          <CardTitle className="pull-left">{this.state.name}</CardTitle>
        </CardHeader>
        <List>
          <ListGroup fill>
            {groups} 
          </ListGroup>
        </List>
      </Card>
    );
  }
}

export default UserContainer;
