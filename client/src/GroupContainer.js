import React, { Component } from 'react';
import { ListGroup } from 'react-bootstrap';
import { List, Card, CardHeader, CardTitle } from 'react-bootcards';
import User from './User';

class GroupContainer extends Component {
  constructor (props) {
    super(props);
    this.state = {
      name: props.name,
      users: props.users
    }
  }

  render () {
    let users = [];

    if (this.state.users) {
      users = (
        <div>
          {this.state.users.map(user => 
            <User key={user._id} email={user.email} name={user.name} />
          )}
        </div>
      );
    }

    return (
      <Card>
        <CardHeader className="clearfix">
          <CardTitle className="pull-left">{this.state.name}</CardTitle>
        </CardHeader>
        <List>
          <ListGroup fill>
            {users} 
          </ListGroup>
        </List>
      </Card>
    );
  }
}

export default GroupContainer;
