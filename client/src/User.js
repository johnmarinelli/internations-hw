import React, { Component } from 'react';
import { ListGroupItem, Button, Row, Col } from 'react-bootstrap';
import {ListGroupItemHeader, ListGroupItemText} from 'react-bootcards';

class User extends Component {

  constructor (props) {
    super(props);
    this.state = {
      name: props.name,
      email: props.email
    }
  }

  render () {
    return (
      <ListGroupItem href="#">
        <Row>
          <Col sm={4}>
            <img alt="profile image" src="https://placehold.it/200x200"/>
          </Col>
          <Col sm={4}>
            <i className="fa fa-3x fa-building pull-left" />
            <ListGroupItemHeader>{this.state.name}</ListGroupItemHeader>
            <ListGroupItemText>{this.state.email}</ListGroupItemText>
          </Col>
          <Col sm={4}>
            <Button bsStyle="primary" className="pull-right">
              <i className="fa fa-arrows-v" />
              Move Group
            </Button>
          </Col>
        </Row>
      </ListGroupItem>
    );
  }
}

export default User;
