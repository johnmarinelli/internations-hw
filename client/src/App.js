import React, { Component } from 'react';
import {Row, Col, Button} from 'react-bootstrap';
import './App.css';
import GroupContainer from './GroupContainer';
import UserContainer from './UserContainer';

class App extends Component {
  constructor (props) {
    super(props);
    this.state = {
      groupsAndUsers: [],
      usersAndGroups: [],
      view: 'bygroup'
    }
  }

  fetchData = () => {
    let view = this.state.view;
    if ('bygroup' === view) {
      fetch('/api/groups?includeUsers=true')
        .then(res => res.json())
        .then(groupsAndUsers => this.setState({ groupsAndUsers }));
    }
    else if ('byuser' === view) {
      fetch('/api/users?includeGroups=true')
        .then(res => res.json())
        .then(usersAndGroups => this.setState({ usersAndGroups }));
    }
  }

  renderData = () => {
    let view = this.state.view;
    let data = [];

    if ('bygroup' === view) {
      data = this.state.groupsAndUsers.map((groupAndUsers) => {
        return (<GroupContainer key={groupAndUsers._id}
          name={groupAndUsers.name}
          users={groupAndUsers.groupUsers } />);
      });
    }

    else if ('byuser' === view) {
      data = this.state.usersAndGroups.map((userAndGroups) => {
        return (<UserContainer key={userAndGroups._id}
          name={userAndGroups.name}
          groups={userAndGroups.userGroups } />);
      });
    }

    return data;
  }

  toggleView = (event) => {
    let currentView = this.state.view;

    this.setState({
      view: currentView === 'bygroup' ? 'byuser' : 'bygroup'
    }, (state) => {
      this.fetchData();
    });

  }

  componentDidMount () {
    this.fetchData();
  }

  render () {
    let data = this.renderData();

    return (
      <div className="App">
        <Row>
          <Col sm={12}>
            <Button bsStyle="primary" className="pull-right" onClick={this.toggleView}>
              <i className="fa fa-arrows-circle" />
              View: {this.state.view}
            </Button>
          </Col>
        </Row>
        <Row>
          {data}
        </Row>
      </div>
    );
  }
}

export default App;
