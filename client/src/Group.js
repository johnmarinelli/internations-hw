import React, { Component } from 'react';
import { ListGroupItem, Button, Row, Col } from 'react-bootstrap';
import {ListGroupItemHeader, ListGroupItemText} from 'react-bootcards';

class Group extends Component {

  constructor (props) {
    super(props);
    this.state = {
      name: props.name
    }
  }

  render () {
    return (
      <ListGroupItem href="#">
        <Row>
          <Col sm={4}>
            <img src="https://placehold.it/200x200"/>
          </Col>
          <Col sm={4}>
            <i className="fa fa-3x fa-building pull-left" />
            <ListGroupItemHeader>{this.state.name}</ListGroupItemHeader>
          </Col>
          <Col sm={4}>
            <Button bsStyle="warning" className="pull-right">
              <i className="fa fa-arrows-v" />
              Remove Group
            </Button>
          </Col>
        </Row>
      </ListGroupItem>
    );
  }
}

export default Group;
